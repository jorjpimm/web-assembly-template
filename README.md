Web Assembly WebGL example
==========================

Local development
-----------------

In order to setup a development environment:
```
> docker run --rm -it -v web-assembly-template/:/src/ apiaryio/emcc bash
```

Then run:
```
> cmake -DCMAKE_TOOLCHAIN_FILE=/emscripten/cmake/Modules/Platform/Emscripten.cmake ..
> make -j
```

See output in `dist/` or https://jorjpimm.gitlab.io/web-assembly-template/
